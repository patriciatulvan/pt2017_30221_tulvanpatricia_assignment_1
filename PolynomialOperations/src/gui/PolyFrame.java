package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import polynomial.Monom;
import polynomial.Polynomial;

import java.awt.Font;

@SuppressWarnings("serial")
public class PolyFrame extends JFrame {

	public PolyFrame() {
		setTitle("Polynomial Calculator");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 300);
		init();
	}
	
	// initializeaza elementele ferestrei 
	public void init(){
		JPanel bigPanel = new JPanel(); // contine toate celelalte componente 
		bigPanel.setBackground(new Color(216, 191, 216));
		bigPanel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), new BevelBorder(BevelBorder.RAISED, null, null, null, null)));
		
		JTextField firstPoly= new JTextField();
		firstPoly.setColumns(30);
	    JTextField secondPoly= new JTextField();
	    secondPoly.setColumns(30);
	    JTextField resultPoly= new JTextField();
	    resultPoly.setColumns(30);
	    
	    JLabel lblFirstPoly = new JLabel("Give the first polynomial: ");
		lblFirstPoly.setFont(new Font("Book Antiqua", Font.BOLD, 14));
	    
	    JLabel lblSecondPoly = new JLabel("Give the second polynomial:");
		lblSecondPoly.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel lblResultPoly = new JLabel("The resulted polynomial:");
		lblResultPoly.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		
		
		JPanel panelFirstPoly = new JPanel();
		panelFirstPoly.setBackground(new Color(255, 245, 238));
		panelFirstPoly.setSize(500, 500);
		panelFirstPoly.add(lblFirstPoly);
		panelFirstPoly.add(firstPoly);
		panelFirstPoly.setVisible(true);
		
		bigPanel.add(panelFirstPoly);
		
		JPanel panelSecPoly = new JPanel();
		panelSecPoly.setBackground(new Color(255, 245, 238));
		panelSecPoly.setSize(500, 500);
		panelSecPoly.add(lblSecondPoly);
		panelSecPoly.add(secondPoly);
		panelSecPoly.setVisible(true);
		
		bigPanel.add(panelSecPoly);
		
		JPanel panelButtons = new JPanel();
		panelButtons.setBackground(new Color(255, 245, 238));
		panelButtons.setSize(500, 500);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text= firstPoly.getText();
				String text1= secondPoly.getText();
				
				if(text.isEmpty() || text1.isEmpty())
					JOptionPane.showMessageDialog(null, "Insert 2 polynomials!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
				else{
					Polynomial p= new Polynomial(transform(text));
					Polynomial q= new Polynomial (transform(text1));
				
					Polynomial r=p.add(q);
					resultPoly.setText(r.printPolynomial());
				}
			}
		});
		btnAdd.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelButtons.add(btnAdd);
		
		JButton btnSubstract = new JButton("Subtract");
		btnSubstract.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text= firstPoly.getText();
				String text1= secondPoly.getText();
				
				if(text.isEmpty() || text1.isEmpty())
					JOptionPane.showMessageDialog(null, "Insert 2 polynomials!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
				else{
					Polynomial p= new Polynomial(transform(text));
					Polynomial q= new Polynomial (transform(text1));
				
					Polynomial r=p.subtract(q);
					resultPoly.setText(r.printPolynomial());
				}
			}
		});
		btnSubstract.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelButtons.add(btnSubstract);
		
		JButton btnMultiply = new JButton("Multiply");
		btnMultiply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text= firstPoly.getText();
				String text1= secondPoly.getText();
				
				if(text.isEmpty() || text1.isEmpty())
					JOptionPane.showMessageDialog(null, "Insert 2 polynomials!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
				else{
					Polynomial p= new Polynomial(transform(text));
					Polynomial q= new Polynomial (transform(text1));
				
					Polynomial r=p.multiply(q);
					resultPoly.setText(r.printPolynomial());
				}
			}
		});
		btnMultiply.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelButtons.add(btnMultiply);
		
		JButton btnDevide = new JButton("Divide");
		btnDevide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text= firstPoly.getText();
				String text1= secondPoly.getText();
				
				if(text.isEmpty() || text1.isEmpty())
					JOptionPane.showMessageDialog(null, "Insert 2 polynomials!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
				else{
					Polynomial p= new Polynomial(transform(text));
					Polynomial q= new Polynomial (transform(text1));
				
					if(p.getGrade()<q.getGrade())
						JOptionPane.showMessageDialog(null, "The grade of the second polynomial can't be bigger then the grade of the first polynomial!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
					else{
						Polynomial r=p.devide(q);
						resultPoly.setText(r.printPolynomial());
					}
				}
			}
		});
		btnDevide.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelButtons.add(btnDevide);
		
		JButton btnDerivate = new JButton("Derive");
		btnDerivate.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		btnDerivate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text= firstPoly.getText();
				if(text.isEmpty())
					JOptionPane.showMessageDialog(null, "Insert polynomial!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
				else{
					Polynomial p= new Polynomial(transform(text));
					Polynomial r=p.derive();
					resultPoly.setText(r.printPolynomial());
				}
			}
		});
		btnDerivate.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelButtons.add(btnDerivate);
		
		JButton btnIntegrate = new JButton("Integrate");
		btnIntegrate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text= firstPoly.getText();
				if(text.isEmpty())
					JOptionPane.showMessageDialog(null, "Insert polynomial!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
				else{
					Polynomial p= new Polynomial(transform(text));
					Polynomial r=p.integrate();
					resultPoly.setText(r.printPolynomial());
				}
			}
		});
		btnIntegrate.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelButtons.add(btnIntegrate);
		
		panelButtons.setVisible(true);
		bigPanel.add(panelButtons);
		
		
		JPanel panelResPoly = new JPanel();
		panelResPoly.setBackground(new Color(255, 245, 238));
		panelResPoly.setSize(500, 500);
		panelResPoly.add(lblResultPoly);
		panelResPoly.add(resultPoly);
		panelResPoly.setVisible(true);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				firstPoly.setText("");
				secondPoly.setText("");
				resultPoly.setText("");
			}
		});
		btnReset.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		panelResPoly.add(btnReset);
		
		bigPanel.add(panelResPoly);
		
		JLabel img;
		ImageIcon calcImg= new ImageIcon("calc.png");
		img= new JLabel(calcImg);
		bigPanel.add(img);
		
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	// metoda folosita pentru preluarea String-ului dat de utilizator si transformarea acestuia in polinom
	public ArrayList<Monom> transform(String text){
		try{
		String in= text.replaceAll("-", "+-");
		String[] splited= in.split("[\\^x+]+");
		ArrayList<Monom> a= new ArrayList<Monom>();
		for(int i=0;i<splited.length;i+=2)
			a.add(new Monom(Double.parseDouble(splited[i]),Integer.parseInt(splited[i+1])));
		return a;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Wrong input!", "Polynomial Calculator", JOptionPane.ERROR_MESSAGE);
		}
		return null;
		
	}
	
	public static void main(String[] args) {
		new PolyFrame();
	}
}
