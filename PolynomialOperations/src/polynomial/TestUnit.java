package polynomial;

import org.junit.Test; 
import static org.junit.Assert.*;

import java.util.ArrayList; 

public class TestUnit {

	Polynomial p1;
	Polynomial p2;
	Polynomial rez;
	
	public void setUp(){
		String p11 = "1x^2+3x^1+3x^0";
		String p12 = "1x^1+2x^0";
		p1=new Polynomial(transform(p11));
		p2=new Polynomial(transform(p12));
		rez = new Polynomial();
		
	}
	
	@Test
	public void testAdd() 
	{   
		setUp();
		rez = p1.add(p2);
		assertEquals(rez.printPolynomial(),"1.0x^2+4.0x^1+5.0"); 
	}
	
	public void testSub() 
	{
		rez = p1.subtract(p2);
		assertEquals(rez.printPolynomial(),"1.0x^2+2.0x^1+1.0"); 
	} 
	
	public void testMultiply() 
	{   
		rez = p1.multiply(p2);
		assertEquals(rez.printPolynomial(),"1.0x^3+3.0x^2+2.0x^2+3.0x^1+6.0x^1+6.0"); 
	} 
	
	public void testDivide(){
		rez = p1.devide(p2);
		assertEquals(rez.printPolynomial(),"1.0x^1+1.0");
	}

	public void testDivide2(){
		String p11 = "3x^2-6x^1+2x^0";
		String p12 = "1x^1-2x^0";
		p1=new Polynomial(transform(p11));
		p2=new Polynomial(transform(p12));
		rez = p1.devide(p2);
		assertEquals(rez.printPolynomial(),"3.0x^1");
	}
	
	public void testDerive() 
	{   
		rez = p1.derive();
		assertEquals(rez.printPolynomial(),"2.0x^1+3.0x^0"); 
	} 
	
	public void testIntegrate() 
	{   
		rez = p1.integrate();
		assertEquals(rez.printPolynomial(),"0.3333333333333333x^3+1.5x^2+3.0x^1"); 
	} 
	
	public ArrayList<Monom> transform(String text){
		String in= text.replaceAll("-", "+-");
		String[] splited= in.split("[\\^x+]+");
		ArrayList<Monom> a= new ArrayList<Monom>();
		for(int i=0;i<splited.length;i+=2)
			a.add(new Monom(Double.parseDouble(splited[i]),Integer.parseInt(splited[i+1])));
		return a;
		
	}
}